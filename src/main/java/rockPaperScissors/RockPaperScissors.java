package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random ;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");


    public void run() {
        // TODO: Implement Rock Paper Scissors
    	Random generator = new Random();
	String innles = "" ;
	String resultat = "";
	String humaninput = "" ;
	String computerinput = "";
	boolean bryter = true;

	while (true) {
		// human chosing
		if (bryter) {
			System.out.println("Let's play round " + roundCounter);
			}
		bryter = true ;
		System.out.print("Your choice (Rock/Paper/Scissors)?");
		humaninput = readInput(innles);

		roundCounter ++ ;
		// cumputer chosing
		int computertall = generator.nextInt(3);
		computerinput = rpsChoices.get(computertall);
		System.out.print("Human chose " + humaninput + ", computer chose " + computerinput + ". ");

		if (humaninput.compareTo("rock") == 0 ){
			if (computerinput.compareTo("rock") == 0) {
				// tie
				resultat = "It's a tie";
				}
			else if (computerinput.compareTo("paper") == 0) {
				// computer winner
				resultat = "Computer wins!";
				computerScore ++ ;
				}
			else if (computerinput.compareTo("scissors") == 0) {
				// human winner
				resultat = "Human wins!";
				humanScore ++ ;
				}
			}
		else if (humaninput.compareTo("paper") == 0 ){
			if (computerinput.compareTo("rock") == 0 ){
				// Human winner
				resultat = "Human wins!";
				humanScore ++ ;
				}
			else if (computerinput.compareTo("paper") == 0) {
				// tie
				
				resultat = "It's a tie";
				}
			else if (computerinput.compareTo("scissors") == 0 ){
				// Computer winner
				resultat = "Computer wins!";
				computerScore ++ ;
				}
			}
		else if (humaninput.compareTo("scissors") == 0 ){
			if (computerinput.compareTo("rock") == 0) {
				// Computer wins

				resultat = "Computer wins!";
				computerScore ++ ;
				}
			else if (computerinput.compareTo("paper") == 0){
				// Human winner
				resultat = "Human wins!";
				humanScore ++ ;
				}
			else if (computerinput.compareTo("scissors") == 0) {
				// tie
				resultat = "It's a tie";

				}
			}
		else {
			System.out.println("I do not understand " + humaninput + ". Could you try again?");
			bryter = false;
			roundCounter -- ;
			continue;
		     }

                
		System.out.println( resultat);
		System.out.println("Score: human " + humanScore + ", computer " + computerScore);
		
		// Are we finish og do we continue?
		boolean bryter2 = true ;
		while (bryter2) {
			System.out.print("Do you wish to continue playing? (y/n)?");
			humaninput = readInput(innles);
			int statusN = humaninput.compareTo("n");
			int statusY = humaninput.compareTo("y");
			if ( statusN != 0 && statusY != 0 ) {
				System.out.println("I do not understand " + humaninput + ". Could you try again?");
				}
			else {
				bryter2 = false ;
				}

			}
		// humaninput is now "n" or "y"
		int status = humaninput.compareTo("n");

		if (status == 0) {
			System.out.println("Bye bye :)" );
			break; 
			}



		}


    }

    /*
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }


}
